﻿using Domain.Interfaces;
using Domain.Models;
using Infrastructure.Services;
using System;
using System.Threading.Tasks;

namespace StoreApi.Services
{
    public class AddService : IAddService
    {
        private readonly IModifyStoreReposity _modifyStoreRepository;
        private readonly IMessageLoggerService _messageLoggerService;

        public AddService(
            IModifyStoreReposity modifyStoreRepository,
            IMessageLoggerService messageLoggerService) 
        {
            _modifyStoreRepository = modifyStoreRepository ?? throw new ArgumentNullException(nameof(modifyStoreRepository));
            _messageLoggerService = messageLoggerService ?? throw new ArgumentNullException(nameof(messageLoggerService));
        }

        public async Task<Store> AddNewStore(
            Guid storeId, 
            string storeName) 
        {
            try 
            {
                return await _modifyStoreRepository.AddNewStore(
                storeId,
                storeName);
            } 
            catch (Exception exception) 
            {
                _messageLoggerService.Error("Error in AddNewStore in add service", exception);

                throw exception;
            }
        }
    }
}
