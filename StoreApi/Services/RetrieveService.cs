﻿using Domain.Interfaces;
using Domain.Models;
using Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace StoreApi.Services
{
    public class RetrieveService : IRetreiveService
    {
        private readonly IRetrieveStoreRepository _retrieveStoreRepository;
        private readonly IMessageLoggerService _messageLoggerService;

        public RetrieveService(
            IRetrieveStoreRepository retrieveStoreRepository,
            IMessageLoggerService messageLoggerService) 
        {
            _retrieveStoreRepository = retrieveStoreRepository ?? throw new ArgumentNullException(nameof(retrieveStoreRepository));
            _messageLoggerService = messageLoggerService ?? throw new ArgumentNullException(nameof(messageLoggerService));
        }

        public async Task<Store> GetStoreById(Guid storeId) 
        {
            try 
            {
                return await _retrieveStoreRepository.GetStoreById(storeId);
            } 
            catch (Exception exception) 
            {
                _messageLoggerService.Error("Error in GetStoreId in retrival service", exception);

                throw exception;
            }
        }

        public async Task<Store> GetStoreByName(string name) 
        {
            try
            {
                return await _retrieveStoreRepository.GetStoreByName(name);
            }
            catch (Exception exception)
            {
                _messageLoggerService.Error("Error in GetStoreByName in retrival service", exception);

                throw exception;
            }
        }

        public async Task<IEnumerable<Store>> GetAllStores() 
        {
            try
            {
                return await _retrieveStoreRepository.GetAllStores();
            }
            catch (Exception exception)
            {
                _messageLoggerService.Error("Error in GetAllStores in retrival service", exception);

                throw exception;
            }
        }

    }
}
