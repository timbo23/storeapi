﻿using Domain.Interfaces;
using Domain.Models;
using Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace StoreApi.Services
{
    public class UpdateService : IUpdateService
    {
        private readonly IModifyStoreReposity _modifyStoreRepository;
        private readonly IMessageLoggerService _messageLoggerService;

        public UpdateService(
            IModifyStoreReposity modifyStoreRepository,
            IMessageLoggerService messageLoggerService) 
        {
            _modifyStoreRepository = modifyStoreRepository ?? throw new ArgumentNullException(nameof(modifyStoreRepository));
            _messageLoggerService = messageLoggerService ?? throw new ArgumentNullException(nameof(messageLoggerService));
        }

        public async Task<Store> UpdateStoreById(
            Guid storeId, 
            string storeName) 
        {
            try 
            {
                return await _modifyStoreRepository.UpdateStoreById(
                storeId,
                storeName);

            } 
            catch (Exception exception) 
            {
                _messageLoggerService.Error("Error in UpdateStoreById in update service", exception);

                throw exception;
            }
        }
    }
}
