﻿using Domain.Interfaces;
using Domain.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace StoreApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StoreController : ControllerBase
    {
        private readonly IAddService _addService;
        private readonly IRetreiveService _retrieveService;
        private readonly IUpdateService _updateService;

        public StoreController(
            IAddService addService, 
            IRetreiveService retrieveService,
            IUpdateService updateService) 
        {
            _addService = addService ?? throw new ArgumentNullException(nameof(addService));
            _retrieveService = retrieveService ?? throw new ArgumentNullException(nameof(retrieveService));
            _updateService = updateService ?? throw new ArgumentNullException(nameof(updateService));
        }

        [HttpPost("AddStore")]
        public async Task<ActionResult<Store>> Add(string storeName)
        {
            var storeId = Guid.NewGuid();
            var newStore = await _addService.AddNewStore(storeId, storeName);
            return StatusCode(201, newStore);
        }

        [HttpGet("GetStore/{storeId}")]
        public async Task<ActionResult<Store>> Get(Guid storeId)
        {
            var store = await _retrieveService.GetStoreById(storeId);
            return StatusCode(201, store);
        }

        //probably should be put?
        [HttpPost("UpdateStore")]
        public async Task<ActionResult<Store>> Update(Guid storeId, string storeName)
        {
            var updatedStore = await _updateService.UpdateStoreById(storeId, storeName);
            return StatusCode(201, updatedStore);
        }
    }
}
