﻿using Domain.Interfaces;
using Domain.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace StoreApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StoresController : ControllerBase
    {

        private readonly IRetreiveService _retrieveService;

        public StoresController(IRetreiveService retrieveService) 
        {
            _retrieveService = retrieveService;
        }

        [HttpGet("GetStores")]
        public async Task<ActionResult<Store>> Get()
        {
            var storeList = await _retrieveService.GetAllStores();
            return StatusCode(201, storeList);
        }
    }
}
