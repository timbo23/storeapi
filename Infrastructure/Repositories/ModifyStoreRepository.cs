﻿using Domain.Interfaces;
using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure
{
    public class ModifyStoreRepository : IModifyStoreReposity
    {
        public async Task<Store> AddNewStore(Guid storeId, string storeName)
        {
            var store = new Store(storeId, storeName);

            return store;
        }

        public async Task<Store> UpdateStoreById(Guid storeId, string storeName)
        {
            var storeList = await GetStoreList();
            var store = storeList.Where(store => store.StoreId == storeId).FirstOrDefault();

            return store;
        }

        private async Task<IEnumerable<Store>> GetStoreList()
        {
            var storeList = new List<Store>
            {
                new Store(Guid.NewGuid(), "Store 1"),
                new Store(Guid.NewGuid(), "Store 2")
            };

            return storeList;
        }
    }
}
