﻿using Domain.Interfaces;
using Domain.Models;
using Infrastructure.Dtos;
using Infrastructure.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure
{
    public class RetrieveStoreRepository : IRetrieveStoreRepository
    {
        private readonly IMapper _mapper;

        public RetrieveStoreRepository(IMapper mapper) 
        {
            _mapper = mapper;
        }

        public async Task<IEnumerable<Store>> GetAllStores()
        {
            try 
            {
                var storeList = await GetStoreList();
                var stores = new List<Store>();

                foreach (var store in storeList)
                {
                    stores.Add(_mapper.MapDtoToModel(store));
                }

                return stores;
            } 
            catch (Exception exception) 
            {
                throw exception;
            }

        }

        public async Task<Store> GetStoreById(Guid storeId)
        {
            try 
            {
                var storeList = await GetStoreList();
                var store = storeList
                    .Where(Store => Store.StoreId == storeId)
                    .FirstOrDefault();
                
                return _mapper.MapDtoToModel(store);
            } 
            catch (Exception exception) 
            {
                throw exception;
            }
        }

        public async Task<Store> GetStoreByName(string storeName)
        {
            try
            {
                var storeList = await GetStoreList();
                var store = storeList
                    .Where(Store => Store.Name == storeName)
                    .FirstOrDefault();

                return _mapper.MapDtoToModel(store);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        private async Task<IEnumerable<StoreDto>> GetStoreList()
        {
            var storeList = new List<StoreDto>
            {
                new StoreDto()
                {
                    Id = 1,
                    StoreId = Guid.NewGuid(),
                    Name = "Store 1"
                },
                new StoreDto()
                {
                    Id = 1,
                    StoreId = Guid.NewGuid(),
                    Name = "Store 2"
                },
            };

            return storeList;
        }
    }
}
