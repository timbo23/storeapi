﻿using System;

namespace Infrastructure.Dtos
{
    public class StoreDto
    {
        public int Id { get; set; }
        public Guid StoreId { get; set; }
        public string Name { get; set; }
    }
}
