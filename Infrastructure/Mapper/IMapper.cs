﻿using Domain.Models;
using Infrastructure.Dtos;

namespace Infrastructure.Mapper
{
    public interface IMapper
    {
        Store MapDtoToModel(StoreDto dto);
    }
}
