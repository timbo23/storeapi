﻿using Domain.Models;
using Infrastructure.Dtos;

namespace Infrastructure.Mapper
{
    public class DtoToDomainModelMapper : IMapper
    {
        public Store MapDtoToModel(StoreDto dto)
        {
            return new Store(dto.StoreId, dto.Name);
        }
    }
}
