﻿using System;

namespace Infrastructure.Services
{
    public class MessageLoggerService : IMessageLoggerService
    {
        public void Error(Exception exception)
        {
            throw new NotImplementedException();
        }

        public void Error(string message, Exception exception)
        {
            throw new NotImplementedException();
        }

        public void Information(string message)
        {
            throw new NotImplementedException();
        }

        public void Information(string message, Exception exception)
        {
            throw new NotImplementedException();
        }

        public void Warning(string message)
        {
            throw new NotImplementedException();
        }

        public void Warning(string message, Exception exception)
        {
            throw new NotImplementedException();
        }
    }
}
