﻿using System;

namespace Infrastructure.Services
{
    public interface IMessageLoggerService
    {
        void Error(Exception exception);
        void Error(string message, Exception exception);
        void Information(string message);
        void Information(string message, Exception exception);
        void Warning(string message);
        void Warning(string message, Exception exception);
    }
}
