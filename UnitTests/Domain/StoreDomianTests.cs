﻿using Domain.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;

namespace UnitTests.Domain
{
    [TestClass]
    public class StoreDomianTests
    {
        [TestMethod]
        public void IfStoreIdIsEmpty_ThrowsArgumentNullException() 
        {
            Assert.ThrowsException<ArgumentNullException>(() => new Store(It.IsAny<Guid>(), It.IsAny<string>()));
        }

        [TestMethod]
        public void IfStoreNameIsEmpty_ThrowsArgumentNullException()
        {
            Assert.ThrowsException<ArgumentNullException>(() => new Store(It.IsAny<Guid>(), It.IsAny<string>()));
        }

        [TestMethod]
        public void setsCorrectStoreName() 
        {
            var testName = "Test Store";
            var store = new Store(Guid.NewGuid(), testName);

            Assert.AreEqual(testName, store.StoreName);
        }

        [TestMethod]
        public void setsCorrectStoreId()
        {
            var testId = Guid.NewGuid();
            var store = new Store(testId, "Test Store");

            Assert.AreEqual(testId, store.StoreId);
        }
    }
}
