﻿using Domain.Interfaces;
using Infrastructure.Services;
using StoreApi.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;
using Domain.Models;
using System.Collections.Generic;

namespace UnitTests.Api.Services
{
    [TestClass]
    public class RetrieveServiceTests
    {
        private Mock<IRetrieveStoreRepository> _retrieveStoreRepository;
        private Mock<IMessageLoggerService> _messageLoggerService;

        [TestInitialize]
        public void TestInitialise() 
        {
            _retrieveStoreRepository = new Mock<IRetrieveStoreRepository>();
            _messageLoggerService = new Mock<IMessageLoggerService>();
        }

        [TestMethod]
        public async Task IfErrorIn_GetStoreById_MessageLoggerIsCalled() 
        {

            _retrieveStoreRepository
                .Setup(x => x.GetStoreById(It.IsAny<Guid>()))
                .Throws<Exception>();

            var sut = new RetrieveService(
                _retrieveStoreRepository.Object, 
                _messageLoggerService.Object);

            try 
            {
                var result = await sut.GetStoreById(It.IsAny<Guid>());
            } 
            catch (Exception exception) 
            { 
            }
            
            _messageLoggerService.Verify(m => m.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once);
        }

        [TestMethod]
        public async Task GetStoreById_returns_ExpectedStoreObject() 
        {
            var expectedStore = new Store(Guid.NewGuid(), "Test store");

            _retrieveStoreRepository
                .Setup(x => x.GetStoreById(It.IsAny<Guid>()))
                .Returns(Task.FromResult(expectedStore));

            var sut = new RetrieveService(
                _retrieveStoreRepository.Object,
                _messageLoggerService.Object);

            var result = await sut.GetStoreById(It.IsAny<Guid>());

            Assert.AreEqual(expectedStore, result);
        }

        [TestMethod]
        public async Task IfErrorIn_GetStoreByName_MessageLoggerIsCalled()
        {

            _retrieveStoreRepository
                .Setup(x => x.GetStoreByName(It.IsAny<string>()))
                .Throws<Exception>();

            var sut = new RetrieveService(
                _retrieveStoreRepository.Object,
                _messageLoggerService.Object);

            try
            {
                var result = await sut.GetStoreByName(It.IsAny<string>());
            }
            catch (Exception exception)
            {
            }

            _messageLoggerService.Verify(m => m.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once);
        }

        [TestMethod]
        public async Task GetStoreByName_returns_ExpectedStoreObject()
        {
            var expectedStore = new Store(Guid.NewGuid(), "Test store");

            _retrieveStoreRepository
                .Setup(x => x.GetStoreByName(It.IsAny<string>()))
                .Returns(Task.FromResult(expectedStore));

            var sut = new RetrieveService(
                _retrieveStoreRepository.Object,
                _messageLoggerService.Object);

            var result = await sut.GetStoreByName(It.IsAny<string>());

            Assert.AreEqual(expectedStore, result);
        }

        [TestMethod]
        public async Task IfErrorIn_GetAllStores_MessageLoggerIsCalled()
        {

            _retrieveStoreRepository
                .Setup(x => x.GetAllStores())
                .Throws<Exception>();

            var sut = new RetrieveService(
                _retrieveStoreRepository.Object,
                _messageLoggerService.Object);

            try
            {
                var result = await sut.GetAllStores();
            }
            catch (Exception exception)
            {
            }

            _messageLoggerService.Verify(m => m.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once);
        }

        [TestMethod]
        public async Task GetAllStores_returns_ExpectedStoreObject()
        {
            var expectedStore = new List<Store>
            {
                new Store(Guid.NewGuid(), "Test store 1"),
                new Store(Guid.NewGuid(), "Test store 2")
            };

            _retrieveStoreRepository
                .Setup(x => x.GetAllStores())
                .ReturnsAsync(expectedStore);

            var sut = new RetrieveService(
                _retrieveStoreRepository.Object,
                _messageLoggerService.Object);

            var result = await sut.GetAllStores();

            Assert.AreEqual(expectedStore, result);
        }
    }
}
