﻿using Domain.Interfaces;
using Domain.Models;
using Infrastructure.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using StoreApi.Services;
using System;
using System.Threading.Tasks;

namespace UnitTests.Api.Services
{
    [TestClass]
    public class UpdateServiceTests
    {
        private Mock<IModifyStoreReposity> _modifyStoreRepository;
        private Mock<IMessageLoggerService> _messageLoggerService;

        [TestInitialize]
        public void TestInitialise()
        {
            _modifyStoreRepository = new Mock<IModifyStoreReposity>();
            _messageLoggerService = new Mock<IMessageLoggerService>();
        }

        [TestMethod]
        public async Task IfErrorIn_UpdateStoreById_MessageLoggerIsCalled()
        {

            _modifyStoreRepository
                .Setup(x => x.UpdateStoreById(It.IsAny<Guid>(), It.IsAny<string>()))
                .Throws<Exception>();

            var sut = new UpdateService(
                _modifyStoreRepository.Object,
                _messageLoggerService.Object);

            try
            {
                var result = await sut.UpdateStoreById(It.IsAny<Guid>(), It.IsAny<string>());
            }
            catch (Exception exception)
            {
            }

            _messageLoggerService.Verify(m => m.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once);
        }

        [TestMethod]
        public async Task UpdateStoreById_returns_ExpectedStoreObject() 
        {
            var expectedStore = new Store(Guid.NewGuid(), "Test store");

            _modifyStoreRepository
                .Setup(x => x.UpdateStoreById(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(Task.FromResult(expectedStore));

            var sut = new UpdateService(
                _modifyStoreRepository.Object,
                _messageLoggerService.Object);

            var result = await sut.UpdateStoreById(It.IsAny<Guid>(), It.IsAny<string>());

            Assert.AreEqual(expectedStore, result);
        }
    }
}
