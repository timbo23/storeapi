﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IRetrieveStoreRepository
    {
        Task<Store> GetStoreById(Guid storeId);
        Task<Store> GetStoreByName(string storeName);
        Task<IEnumerable<Store>> GetAllStores();
    }
}
