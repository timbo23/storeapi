﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IModifyStoreReposity
    {
        Task<Store> AddNewStore(Guid storeId, string storeName);
        Task<Store> UpdateStoreById(Guid storeId, string storeName);
    }
}
