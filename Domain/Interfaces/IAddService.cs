﻿using Domain.Models;
using System;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IAddService
    {
        Task<Store> AddNewStore(
            Guid storeId,
            string storeName);
    }
}
