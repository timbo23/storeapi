﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IUpdateService
    {
        Task<Store> UpdateStoreById(
            Guid storeId,
            string storeName);
    }
}
