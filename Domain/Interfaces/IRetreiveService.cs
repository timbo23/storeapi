﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IRetreiveService
    {
        Task<Store> GetStoreById(Guid storeId);
        Task<Store> GetStoreByName(string name);
        Task<IEnumerable<Store>> GetAllStores();
    }
}
