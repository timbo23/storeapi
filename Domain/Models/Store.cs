﻿using System;

namespace Domain.Models
{
    public class Store
    {
        public Guid StoreId { get; }
        public string StoreName { get; }

        public Store(Guid storeId, string storeName) 
        {
            if (!ValidateStoreId(storeId)) 
            {
                throw new ArgumentNullException("Store Id should not be null");
            }

            if (!ValidateStoreName(storeName))
            {
                throw new ArgumentNullException("Store name should not be null");
            }

            StoreId = storeId;
            StoreName = storeName;
        }

        private bool ValidateStoreId(Guid storeId) 
        {
            var isGuid = Guid.TryParse(storeId.ToString(), out var newGuid);
            var isNotEmpty = !String.IsNullOrEmpty(storeId.ToString());

            return isGuid && isNotEmpty;
        }
        
        private bool ValidateStoreName(string storeName) => !String.IsNullOrEmpty(storeName);

    }
}
